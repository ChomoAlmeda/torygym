$(document).ready(main);
$('#Carrera').attr('disabled', true);
$('#labelNombre').hide();
$('#labelContra').hide();

var contador = 1;
var up = 1;
var mod = 1;
var aux = 1;

//muestra elementos al cargar la pagina

function main(){
    $('.menu').click(function(){
    //$('nav').toggle()
    if(contador == 1){
      $('nav').animate({
        left: "0", 
        background: 'red',
      });
      contador = 0;
    }else{
      $('nav').animate({
        left: "-100%", 
        background: 'red',
      });
      contador = 1;
    }
  });
};
	$("#Facultad").change(function(){
		if($(this).val()!=""){
			var dato=$(this).val();
				$("#Carrera").empty();								
				$("#Carrera").css("padding","0");
				$.ajax({
					type:"POST",
					dataType:"html",
					url:"http://localhost/emprendedores/index.php/inicio/carreras/"+dato,
					success:function(msg){
						$("#Carrera").empty().removeAttr("disabled").append(msg);
					}
				});
		}else{
			$("#Carrera").empty().attr("disabled","disabled");
		}
	});

$('#Fecha').datepicker({dateFormat:'yy-mm-dd'});

$('article').delay(2000).animate({left:"0%"},1000, "easeOutExpo");
//graficas
	google.charts.load('current', {'packages':['corechart']});
    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawUnoChart);
    google.charts.setOnLoadCallback(drawDosChart);
    google.charts.setOnLoadCallback(drawTresChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawUnoChart() {
     // Create the data table.
    var data = new google.visualization.DataTable();
       data.addColumn('string', 'Topping');
       data.addColumn('number', 'Slices');
       data.addRows([
        [' >= 30', 3],
        ['= 30', 1],
        ['>= 25 <= 30', 1],
        ['<= 20', 1]
    ]);
    // Set chart options
    var options = {'title':'Usuarios Registrados',
            'width':290,
            pieHole: 0.2,
            'height': 220,
            is3D : true
            };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }

       function drawDosChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          [' >= 30', 3],
          ['= 30', 1],
          ['>= 25 <= 30', 1],
          ['<= 20', 1]
        ]);

        // Set chart options
        var options = {'title':'Usuarios Registrados',
                       'width':300,
                       pieHole: 0.2,
                       'height': 220,
                       is3D : true
                   };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div2'));
        chart.draw(data, options);
      }

       function drawTresChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          [' >= 30', 3],
          ['= 30', 1],
          ['>= 25 <= 30', 1],
          ['<= 20', 1]
        ]);

        // Set chart options
        var options = {'title':'Usuarios Registrados',
                       'width':300,
                       pieHole: 0.2,
                       'height': 220,
                       is3D : true
                   };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div3'));
        chart.draw(data, options);
      }

